import sqlalchemy
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String
from database import db

class Kategorie(db.Model):
    __tablename__ = 'Kategorien'
    id = Column(Integer, primary_key= True)
    name = Column(String)
    beschreibung = Column(String)

class Angebot(db.Model):
    def genuuid():
        import string
        import random
        allchar= string.ascii_letters + string.digits
        uuid ="".join(random.choice(allchar) for x in range(99))
        return uuid

    __tablename__= 'Angebote'
    id = Column(Integer, primary_key = True)
    name = Column(String)
    kategorie_id = Column(Integer, ForeignKey('Kategorien.id'))
    kategorie = relationship("Kategorie")
    beschreibung = Column(String)
    kurzbeschreibung = Column(String)
    wo = Column(String)
    wann = Column(String)
    uuid = Column(String, default=genuuid())

class Anmeldung(db.Model):
    __tablename__ = 'Anmeldungen'
    id = Column(Integer, primary_key = True)
    name = Column(String)
    angebot_id = Column(Integer, ForeignKey('Angebote.id'))
    angebot = relationship("Angebot")


sport_Kategorien = Kategorie(name='Sport', beschreibung='Finde hier Sportbegeisterte')
kultur_Kategorien = Kategorie(name='Kultur', beschreibung='Andere Kulturbegeisterte warten schon auf dich!')
it_und_technik_Kategorien = Kategorie(name='IT und Technik', beschreibung='Finde andere Alpakas')
musik_Kategorien = Kategorie(name='Musik', beschreibung='Finde hier Musikbegeisterte')

angebot1_Angebot = Angebot(name='Klettern mit Nic', kategorie=sport_Kategorien, beschreibung='Hey, ich habe Lust mit netten Menschen klettern zu gehen. Meldet euch bei Interesse!', kurzbeschreibung='Ich bin Nic und gehe in meiner Freizeit gerne in die Natur :)', wo='Boulderplöcke am Thüringer Bahnhof', wann='Freitag ab 15 Uhr')
angebot2_Angebot = Angebot(name='Gitarristen gesucht!', kategorie=musik_Kategorien, beschreibung='Hallo, ich will mit anderen eine Band gründen. Ich suche Gitarrenspieler. Sagt Bescheid, ob ihr kommt!', kurzbeschreibung='Ich bin Luise, 16 Jahre alt und spiele seit 3 Jahren Schlagzeug.', wo='In meiner Garage', wann='Samstag um 10 Uhr')
angebot3_Angebot = Angebot(name='Programmiert mit mir!', kategorie=it_und_technik_Kategorien, beschreibung='Moin! Ich such andere Alpakas, die mit mir programmieren.', kurzbeschreibung='Ich bin Torsten und 50 Jahre alt', wo='In der Welt der Alpakas!', wann='Immer')
angebot4_Angebot = Angebot(name='Wöchentliche Kunstaktivitäten', kategorie=kultur_Kategorien, beschreibung='Ich suche Kunstbegeisterte um meine wöchentlichen Museums- und Ausstellungsbesuche etwas geselliger zu gestalten. Ich freu mich dich bald kennenzulernen!', kurzbeschreibung='Mein Name ist Nicole und ich studiere momentan Kunst und Deutsch auf Lehramt.', wo='Verschiedene Museen in Halle', wann='Dienstags von 14 Uhr bis 16:30 Uhr')
angebot5_Angebot = Angebot(name='Fußballmannschaft gesucht...', kategorie=sport_Kategorien, beschreibung='Nach einigen Jahren Pause würde ich jetzt gerne wieder anfangen Fußball zu spielen. Es wäre wirklich super wenn sich eine Trainingsgruppe bildet!', kurzbeschreibung='Als Kind habe ich in einem Amateurverein Fußball gespielt. Ich heiße Lina, bin 19 Jahre alt und mache gerade ein FSJ.', wo='Stadion Halle', wann='Am Wochenende')
angebot6_Angebot = Angebot(name='Nordische Volkstänze', kategorie=kultur_Kategorien, beschreibung='Ich suche gut gelaunte Damen, die mit mir nordische Volkstänze tanzen wollen. Schreiben sie mich doch gerne über diese Plattform an.', kurzbeschreibung='Ich bin die Beate und bin 70 Jahre alt.', wo='Großer Saal des Generationenhauses Halle', wann='Montags und Donnerstags')


def allAngebote(session):
    Angebote=session.query(Angebot).all()
    return Angebote

def allKategorien(session):
    Kategorien=session.query(Kategorie).all()
    return Kategorien

def AngeboteFuerKategorie(session, id):
    Angebote=session.query(Angebot).filter_by(kategorie_id=id).all()
    return Angebote

def AnmeldungenFuerAngebot(session, id, authtoken):
    uuid = session.query(Angebot).filter_by(id=id).add_columns(Angebot.uuid)
    if uuid == authtoken:
        Anmeldung=session.query(Anmeldung).filter_by(angebot_id=id).all()
        return Anmeldung

def AngebotFuerId(session, id):
    return session.query(Angebot).filter_by(id=id).one()

def createAngebot(session, name, kategorie_id, beschreibung, kurzbeschreibung, wo, wann):
    session.add(Angebot(name=name, kategorie_id=kategorie_id, beschreibung=beschreibung, kurzbeschreibung=kurzbeschreibung, wo=wo, wann=wann))
    session.commit()

def create_objects(session):
    session.add(sport_Kategorien)
    session.add(kultur_Kategorien)
    session.add(it_und_technik_Kategorien)
    session.add(musik_Kategorien)
    session.commit()
    session.add(angebot1_Angebot)
    session.add(angebot2_Angebot)
    session.add(angebot3_Angebot)
    session.add(angebot4_Angebot)
    session.add(angebot5_Angebot)
    session.add(angebot6_Angebot)
    session.commit()
