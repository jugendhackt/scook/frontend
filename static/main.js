clippy.load('Clippy', function(agent) {
    // Do anything with the loaded agent
    agent.show();
    // play a given animation
    agent.play('Searching');

    // move to the given point, use animation if available
    agent.moveTo(window.innerWidth / 4 + Math.round(Math.random() * (window.innerWidth / 2)), window.innerHeight / 4 + Math.round(Math.random() * (window.innerHeight / 2)));

    // play a random animation
    agent.animate();

    // Show text balloon
    agent.speak('When all else fails, bind some paper together. My name is Clippy. I\'m here to help you.');

    // gesture at a given point (if gesture animation is available)
    agent.gestureAt(200, 200);
});