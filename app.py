#!/usr/bin/env python3
from API import allKategorien, AngeboteFuerKategorie, AngebotFuerId, createAngebot

from flask import Flask, render_template, request, send_from_directory, redirect
from API import allKategorien, create_objects
from database import db

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///:memory:'
db.init_app(app)


@app.route('/static/<path:path>')
def send_js(path):
    return send_from_directory('static', path)

@app.route("/")
def greet():
    return render_template('index.html')


@app.route("/offers")
def offers():
    return render_template('offers.html', categories=allKategorien(db.session))

@app.route("/formular")
def formular():
    return render_template('formular.html', categories=allKategorien(db.session))

@app.route("/createActivity")
def createActivity():
    name=request.args.get("name")
    kategorie_id=request.args.get("kategorie_id")
    beschreibung=request.args.get("beschreibung")
    kurzbeschreibung=request.args.get("kurzbeschreibung")
    wo=request.args.get("wo")
    wann=request.args.get("wann")
    createAngebot(db.session, name, kategorie_id, beschreibung, kurzbeschreibung, wo, wann)
    return redirect("/", code=302)

@app.route("/activity")
def activity():
    return render_template('activity.html')

@app.route("/category/<int:post_id>")
def activitylist(post_id):
    return render_template('activitylist.html',events=AngeboteFuerKategorie(db.session, post_id))

@app.route("/category/event/<int:post_id>")
def event(post_id):
    return render_template('event.html', event=AngebotFuerId(db.session, post_id))

if __name__ == "__main__":
    with app.app_context():
        db.create_all()
        create_objects(db.session)
    app.run()
